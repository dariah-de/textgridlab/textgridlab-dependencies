# TextGridLab dependencies

This project is used to package existing libraries (OSGi bundles) deployed to some Maven repository for use in the TextGridLab. 

Please see:

* [Libraries in TextGridLab](https://wiki.de.dariah.eu/display/TextGrid/Libraries+in+TextGridLab) for a description and HOWTO of the process,
* [The Integration Build Page](https://ci.de.dariah.eu/jenkins/view/TextGridLab%20Build/job/lab-dependencies/) for status and history of the project, and also for a link to the p2 update site to include the aggregated libraries.

## Change Version

To change the version use the [Tycho versions plugin](https://www.eclipse.org/tycho/sitedocs/tycho-release/tycho-versions-plugin/set-version-mojo.html)

	mvn tycho-versions:set-version -DnewVersion=4.0.0-SNAPSHOT
	
also edit `base-repository/info.textgrid.lab.core.application.base_product.product` then to set the version
